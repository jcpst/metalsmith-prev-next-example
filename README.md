# Metalsmith 'previous' and 'next' links example

Metalsmith is a pretty cool platform, but the documentation is spotty. 
This about the most basic example I could come up with to show how to create 'previous' and 'next' links on a static blog.

After you clone the repo...

Install dependencies

	npm i

Build the static site and start the server

    npm start

Then head to <http://0.0.0.0:8080>

You can just rebuild the static site with

    node .
